var express = require('express');
var fs = require('fs');
var sharedsession = require("express-socket.io-session");
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var _ = require('underscore');
var fs      = require('fs');
var gm = require('gm');
var exec = require('child_process').exec;
var PDFDocument = require('pdfkit');

var app = express();
var http = require('http').Server(app);
var participants = {};
var classroom = "";
var cloudinary = require('cloudinary');

cloudinary.config({ 
  cloud_name: "sensemagiktest", 
  api_key: "646728953562877", 
  api_secret: "vh-bfPW8JqG7R0p2U8h31VRE6_E"
});

var config = {};
config.ip = process.env.OPENSHIFT_NODEJS_IP || '45.33.68.207';
config.port = process.env.OPENSHIFT_NODEJS_PORT || 3000;

var server = app.listen(config.port, config.ip, function() {
  console.log('%s: Node server started on %s:%d ...',
  Date(Date.now() ), config.ip, config.port);
});
var session = require("express-session")({
  secret: "lessonexpert",
  resave: true,
  saveUninitialized: true
});

var options = {
    tmpDir:  __dirname + '/public/uploaded/tmp',
    uploadDir: __dirname + '/public/uploaded/files',
    uploadUrl:  '/uploaded/files/',
    maxPostSize: 11000000000, // 11 GB 
    minFileSize:  1,
    maxFileSize:  10000000000, // 10 GB 
    acceptFileTypes:  /.+/i,
    // Files not matched by this regular expression force a download dialog, 
    // to prevent executing any scripts in the context of the service domain: 
    //inlineFileTypes:  /\.(gif|jpe?g|png)/i,
    //imageTypes:  /\.(gif|jpe?g|png)/i,
    //copyImgAsThumb : true, // required 
    /*imageVersions :{
        maxWidth : 200,
        maxHeight : 200
    },*/
    accessControl: {
        allowOrigin: '*',
        allowMethods: 'OPTIONS, HEAD, GET, POST, PUT, DELETE',
        allowHeaders: 'Content-Type, Content-Range, Content-Disposition'
    },
    storage : {
        type : 'local'
    }
};
var uploader = require('blueimp-file-upload-expressjs')(options);

//session.handshake.session.users = [];
/*
app.use(session({
  secret: 'lessonexpert',
  resave: true,
  saveUninitialized: true
}));
*/
var io = require('socket.io').listen(server);
//io.connect("lessonexpert-goalship.rhcloud.com:8000");
// view engine setup
app.engine('html', require('ejs').renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session);

// Share session with io sockets

io.use(sharedsession(session));

//socket io 

io.on('connection', function(socket){
  socket.on('create',function(room){
    console.log(room);
    classroom = room;
    console.log("classroom is "+classroom);
    socket.join(room);
  });

  socket.on('whiteboard', function(msg){
    console.log('whiteboard for class : ' + classroom);
     io.in(classroom).emit('whiteboard', msg);
  });

  socket.on("newUser", function(data) {
    console.log('participants called for class '+data.classroom);
    //participants.push({id: data.id, name: data.name});
    if(!participants[data.classroom]){
      participants[data.classroom] = [];
      participants[data.classroom].push({id: data.id, name: data.name});
    }else{
      participants[data.classroom].push({id: data.id, name: data.name});
    }
    //io.sockets.emit("newConnection", {participants: participants});
    console.log(participants);
    io.in(data.classroom).emit('user connected',participants[data.classroom]);
  });
  
  socket.on("getParticipants",function(classname){
    io.in(classname).emit('user connected',participants[classname]);
  });

  socket.on("disconnect",function(){
    console.log("disconnecting from classroom "+classroom);
    //classs = _.each(participants,_.findWhere())
    /*
    _.each(participants,function(x){
      //_.findWhere(classname,{id: socket.id})
      console.log(x);
      //var classnames = _.findWhere(x,{id:socket.id});
      //console.log(classnames);
    });
    participants[classroom] = _.without(participants[classroom],_.findWhere(participants[classroom], {id: socket.id}));
    io.in(classroom).emit('user connected',participants[classroom]);
    console.log("------disconnected");
    console.log(participants);*/
    var foundkey;
    for(var key in participants){
      participants[key].forEach(function(f){
        //console.log(f);
        if(f.id == socket.id){
          foundkey = key;
          console.log('found in '+key)
        }
      });
    }
    participants[foundkey] = _.without(participants[foundkey],_.findWhere(participants[foundkey],{id: socket.id}));
    console.log(participants);
    io.in(foundkey).emit('user connected',participants[foundkey]);
  });

  socket.on("raiseHand",function(data){
    io.in(classroom).emit('raiseHand',data);
  });

  socket.on("attendRaise",function(user){
    io.in(classroom).emit("attendRaise",user);
  });

  socket.on("activateUser",function(user){
    io.in(classroom).emit("activateUser",user);
  });

  socket.on("changeAudio",function(control){
    io.in(classroom).emit("changeAudio",control);
  });

  socket.on("changeVideo",function(control){
    io.in(classroom).emit("changeVideo",control);
  });

  socket.on("chatmessage",function(message){
    io.in(message.classroom).emit("chatmessage",message);
  });

  socket.on("filesuploaded",function(files){
    io.in(classroom).emit("filesuploaded",files);
  });
});

//routes
/* GET home page. */
app.get('/',function(req,res){
  res.render('home.html',{ basePath: 'views/'});
});
app.get('/video',function(req,res){
  if(req.query.user == 'admin'){
    res.render('video.html', { basePath: 'views/' ,user: 'admin'});

  }else{
    res.render('video.html', { basePath: 'views/' ,user: req.query.user});
  }

});

app.post('/upload',function(req,res,next){
  //console.log(req.body);
  console.log("uploadrequest")
  uploader.post(req,res,function(err,obj){
    if(!err){
      console.log("file uploaded");
      res.send(JSON.stringify(obj));
    }
  })
});

app.post('/pdfupload',function(req,res,next){
  uploader.post(req,res,function(err,obj){
    if(!err){
      var file=__dirname+"/public/uploaded/files/"+obj.files[0].name;
      cloudinary.uploader.upload(file, function(result) { 
        console.log(result) 
        res.send(result);
      });
    }
  })
});

app.get('/board', function(req, res) {
  var session = req.session;
  console.log(req.query);
  console.log('io');
  if(req.query.user == 'teacher'){
    res.render('index.html', { basePath: 'views/' ,user: 'teacher', className: req.query.class, ip: config.ip, port: config.port});

  }else{
    res.render('index.html', { basePath: 'views/' ,user: req.query.user, className: req.query.class, ip: config.ip, port:config.port});
  }
});

app.post('/board',function(req,res){
  var session=req.session;
  if(req.body.user == 'teacher'){
    res.render('index.html', { basePath: 'views/' ,user: 'teacher', className: req.query.class, ip: config.ip, port: config.port});
  }else{
    res.render('index.html', { basePath: 'views/' ,user: req.query.user, className: req.query.class, ip: config.ip, port:config.port});
  }
});

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
