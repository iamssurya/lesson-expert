// grab the room from the URL
var room = className;
var isVideoMuted = false;
var isMicMuted = false;
//var room = "<%=user%>" || "student";
// create our webrtc connection
$('#localVideo').addClass("uservideo_"+role);

var webrtc = new SimpleWebRTC({
    // the id/element dom element that will hold "our" video
    localVideoEl: 'localVideo',
    // the id/element dom element that will hold remote videos
    remoteVideosEl: '',
    // immediately ask for camera access
    autoRequestMedia: true,
    debug: false,
    nick: role,
    detectSpeakingEvents: true,
    autoAdjustMic: false
    //peerConnectionConfig: {'iceServers': [{'url': 'stun:stun.services.mozilla.com'}, {'url': 'stun:stun.l.google.com:19302'}]}
});

webrtc.on('readyToCall', function () {
    if (room) {
        webrtc.joinRoom(room);
        if(role != "teacher"){
           //webrtc.pause();
           // webrtc.mute();
            console.log("paused");
        }else{
          //  webrtc.mute();
        }
    }
});

if(role != "teacher"){
    /*
    webrtc.pause();
    webrtc.mute();*/
}

function showVolume(el, volume) {
    if (!el) return;
    if (volume < -45) { // vary between -45 and -20
        el.style.height = '0px';
    } else if (volume > -20) {
        el.style.height = '100%';
    } else {
        el.style.height = '' + Math.floor((volume + 100) * 100 / 25 - 220) + '%';
    }
}
webrtc.on('channelMessage', function (peer, label, data) {
   // console.log("teacher");
   // console.log(data);
    if (data.type == 'volume') {
        //showVolume(document.getElementById('volume_' + peer.id), data.volume);
    }
});
webrtc.on('videoAdded', function (video, peer) {
    console.log("peer");
    console.log(peer);
    console.log('video added', peer.nick);
    if(peer.nick == 'teacher'){
        //$(".teacher-video").append(video);
        $(".teacher-video").append("<div id='container_"+webrtc.getDomId(peer)+"'></div>");
        $("#container_"+webrtc.getDomId(peer)).addClass("uservideo_"+peer.nick).append(video);
    }
    else{
        var remotes = document.getElementById('remotes');
        if (remotes) {
            $("#remotes").append("<div class=videoContainer id='container_"+webrtc.getDomId(peer)+"'></div>");
            $("#container_"+webrtc.getDomId(peer)).addClass("uservideo_"+peer.nick).append(video).css('display','none').attr("muted");
            $("#"+webrtc.getDomId(peer)).attr("muted",'');
        }
    }
});
webrtc.on('videoRemoved', function (video, peer) {
    $("#container_"+webrtc.getDomId(peer)).remove();
});

webrtc.on('volumeChange', function (volume, treshold) {
    //console.log('own volume', volume);
    showVolume(document.getElementById('localVolume'), volume);
});

var pauseOrResumeVideo = function(){
    if(!isVideoMuted){
        webrtc.pause();
        console.log('video pausing');
        $('#videoButton').removeClass('btn-primary').addClass('btn-danger'); 
        isVideoMuted = true;
    }else{
        webrtc.resume();
        $('#videoButton').removeClass('btn-danger').addClass('btn-primary'); 
        isVideoMuted = false;
    }
}

var pauseOrResumeMic = function(){
    if(!isMicMuted){
        webrtc.mute();
        $('#micButton').removeClass('btn-primary').addClass('btn-danger'); 
        isMicMuted = true;
    }else{
        webrtc.unmute();
        $('#micButton').removeClass('btn-danger').addClass('btn-primary'); 
        isMicMuted = false;
    }
}


