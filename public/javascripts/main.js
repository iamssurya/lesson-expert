var socket = io.connect(ip+":"+port);
var room = className;
var usersList = [];
var tempUser;
var zoomlevel = 1;
var pages,current_page,version,public_id;

$(".pdf-options").hide();
socket.emit('create',className);

socket.on('connect',function(data){
	var sessionId = socket.io.engine.id;
	console.log('Connected ' + sessionId); 
	socket.emit('newUser',{id: sessionId,name: role,classroom:className});
});
socket.emit('getParticipants',className);

var options={
	imageURLPrefix: 'javascripts/literallycanvas/img'
}
var lc = LC.init($('.literally').get(0), options);
var lc2 = LC.init($('.literally2').get(0), options);

//http://res.cloudinary.com/sensemagiktest/image/upload/v1451049035/phpszvuhqeiznbgqzemc.pdf
//lc.saveShape(LC.createShape('Pdf',{x: 40,y: 10,pages: 2,version: "1451047716",public_id: "szrcrmjbjzcimkpspuar"}));
/*
for(var i=0;i<2;i++){
	
}

var newImage = new Image();
newImage.src = 'http://res.cloudinary.com/sensemagiktest/image/upload/w_800,h_1050,c_fill,pg_1/v1451049035/phpszvuhqeiznbgqzemc.jpg';
console.log(newImage.src);
lc.saveShape(LC.createShape('Image', {x: 40, y: 10, image: newImage}));
*/
function previousPage(){
	if(current_page-1 > 0){
		current_page = current_page - 1;
		var newImage = new Image();
		newImage.src = 'http://res.cloudinary.com/sensemagiktest/image/upload/pg_'+current_page+'/v'+version+'/'+public_id+'.jpg';
		lc.saveShape(LC.createShape('Image', {x: 40, y: 10, image: newImage}));
	}
}
function nextPage(){
	if(current_page < pages){
		current_page = current_page+1;
		var newImage = new Image();
		newImage.src = 'http://res.cloudinary.com/sensemagiktest/image/upload/pg_'+current_page+'/v'+version+'/'+public_id+'.jpg';
		lc.saveShape(LC.createShape('Image', {x: 40, y: 10, image: newImage}));
	}
	
}
function zoom(type){
	if(type == 'in'){
		zoomlevel = zoomlevel + 0.3;
		lc.setZoom(zoomlevel);

	}else{
		zoomlevel = zoomlevel - 0.3;
		lc.setZoom(zoomlevel);
	}
}

lc.on('drawingChange', function() {
  var json  = JSON.stringify(lc.getSnapshot());
  socket.emit('whiteboard', json);
});

lc.on('clear',function(){
	$(".pdf-options").hide();
})

socket.on('whiteboard',function(msg){
	//console.log(msg);
	//console.log('whiteboard event received');
	lc2.loadSnapshot(JSON.parse(msg));
});

//socket.emit('user connected',user);
socket.on('user connected',function(users){
	console.log("user connected message");
	console.log(users);
	$('#users').empty();
	var buffer = "";
	$.each(users,function(index,val){
		//console.log(val);
		
		if(val.name != 'teacher')
		{
			var username = '"'+val.name+'"';
			buffer+="<a id='user_"+val.name+"' class=list-group-item><span class='glyphicon glyphicon-user list-addon'></span>"+val.name+" <span class='fa fa-hand-stop-o list-addon' style='margin-left:20%;' onclick='attendRaise("+username+")'></span> <input type='checkbox' style='margin-left: 6%;' id='audio_"+username.replace(/["']/g, '')+"' onchange='activateAudio("+username+")' /> <span class='fa fa-microphone list-addon'></span> <input type='checkbox' style='margin-left: 7%;' id='video_"+username.replace(/["']/g, '')+"' onchange='activateVideo("+username+")'/><span class='fa fa-video-camera list-addon'></span></a>";
		}
		$('#users').html(buffer);
	});
});

socket.on('disconnect',function(){
	socket.emit('disconnect',className);
});

// Importing to Canvas (Image/ Pdf)
var uploadImage = function(){
	$('#pdfupload').click();
}

var loadPdfCanvas = function(data){
	console.log("inside fun");
	console.log(data);
	pages = data.pages;
	current_page = 1;
	version = data.version;
	public_id = data.public_id;
	var newImage = new Image();
	newImage.src = 'http://res.cloudinary.com/sensemagiktest/image/upload/pg_1/v'+data.version+'/'+data.public_id+'.jpg';
	lc.saveShape(LC.createShape('Image', {x: 40, y: 10, image: newImage}));
	$(".pdf-options").show();
}
//raise Hand

var raiseHand = function(){
	console.log(role);
	socket.emit('raiseHand',{user: role});
}

socket.on('raiseHand',function(data){
	console.log("raisehand"+data.user);
	$('#user_'+data.user).addClass('highlightUser');
});


var attendRaise = function(user){
    console.log(user);
    if(role == 'teacher'){
		$('#user_'+user).removeClass('highlightUser');
		socket.emit("attendRaise",user);
    }
}

socket.on('attendRaise',function(user){
	$('#user_'+user).removeClass('highlightUser');
});

//show for teacher only

if($(".teacher-button").data("onlyTeacher"))
{
	console.log('teacher only');
	$(".teacher-button").addClass("teacher-only");
	if(role == 'teacher'){
		$(".teacher-only").css('display','inline');
	}else{
		$(".teacher-only").css('display','none');
	}
}

//for showing remote user video

var showPeerVideo = function(user){
    socket.emit('activateUser',user);
}

var closeVideo = function(){

}

socket.on("activateUser",function(user){
	console.log('activate user '+user);
	console.log(role);
	webrtc.resume();
	if(user == role){
		//$("#remotes").append("<video id='localVideo'></video>");
		$(".selfVideo").show();
	}else{
		$(".uservideo_"+user).css('display','block');
	}
});

var muteAll = function(){
	if($("#muteAll").prop("checked")){
		webrtc.mute();
	}else{
		webrtc.unmute();
	}
}
var pauseAll = function(){
	if($("#pauseAll").prop("checked")){
		webrtc.pause();
	}else{
		webrtc.resume();
	}
}
var activateAudio = function(user){
	var control = {};
	control.user = user;
	if($("#audio_"+user).prop("checked")){
		control.type = "activate";
		socket.emit("changeAudio",control);
	}else{
		control.type = "deactivate";
		socket.emit("changeAudio",control);
	}
}

var activateVideo = function(user){
	console.log('activatevideo '+user);
	var control = {};
	control.user = user;
	if($("#video_"+user).is(":checked")){
		console.log('video checked');
		control.type = "activate";
		socket.emit("changeVideo",control);
	}else{
		console.log('unchecked');
		control.type = "deactivate";
		socket.emit("changeVideo",control);
	}
}

socket.on("changeAudio",function(control){
	webrtc.resume();
    switch(control.type){
    	case "activateAudio":
    		if(control.user == role){
    			webrtc.unmute();
    		}
    		break;
    	case "deactivateAudio":
    		if(control.user == role){
    			webrtc.mute();
    		}
    		break;
    }
});

socket.on("changeVideo",function(control){
	webrtc.resume();
	console.log(control);
	console.log("changing video for "+control.user);
    switch(control.type){
    	case "activate":
    		$("#video_"+control.user).prop("checked",true);
    		if(control.user == role){
    			webrtc.unmute();
    			webrtc.resumeVideo();
    			$(".selfVideo").show();
    			console.log("activated video for "+control.user);
    		}else{
				$(".uservideo_"+control.user).css('display','block');
				console.log("activated video for "+control.user);
			}
    		break;
    	case "deactivate":
    		$("#video_"+control.user).prop("checked",false);
    		if(control.user == role){
    			webrtc.pauseVideo();
    			$(".selfVideo").hide();
    		}else{
    			$(".uservideo_"+control.user).css('display','none');
    		}
    		break;
    }
});

var sendmessage = function(){
	var message = {from : role, classroom: className};
	if($("#chatmessage").val() != ""){
		message.message = $("#chatmessage").val();
		$("#chatmessage").val('');
		socket.emit("chatmessage",message);
	}
}

socket.on("chatmessage",function(message){
	if(tempUser != message.from){
		$("#chatbox").append("<li><span class='chatuser'><span class='fa fa-user'></span> "+message.from+"</span><span class='chatmess'>"+message.message+"</span></li>");
		tempUser = message.from;
	}else{
		var list = $("<li><span class='chatuser' style='visibility:hidden;'><span class='fa fa-user'></span> "+message.from+"</span><span class='chatmess'>"+message.message+"</span></li>");
		$("#chatbox").append(list);
		$('#chatbox').scrollTop(1E10);
	}
	
});



